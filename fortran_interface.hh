interface

  function eos_tempmin() bind(c)
    use iso_c_binding
    real (c_double) :: eos_tempmin
  end function eos_tempmin

  function eos_yemin() bind(c)
    use iso_c_binding
    real (c_double) :: eos_yemin
  end function eos_yemin

  function eos_yemax() bind(c)
    use iso_c_binding
    real (c_double) :: eos_yemax
  end function eos_yemax


end interface
