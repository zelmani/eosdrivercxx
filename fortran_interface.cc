#include <cassert>
#include <string>
#include <cstring>
#include <iostream>
#include "nuc_eos.hh"
using namespace std;

extern "C"
void nuc_eos_full_(const double *rho, 
		   double *temp,
		   const double *ye,
		   double *eps,
		   double *prs,
		   double *ent,
		   double *cs2,
		   double *dedt,
		   double *dpderho,
		   double *dpdrhoe,
		   double *xa,
		   double *xh,
		   double *xn,
		   double *xp,
		   double *abar,
		   double *zbar,
		   double *mue,
		   double *mun,
		   double *mup,
		   double *muhat,
		   int *keytemp,
		   int *keyerr,
		   double *freps){
  
  int anyerr=0;
  int n_in=1;

  if(*keytemp<0 || *keytemp>2) cout << *keytemp << endl;
  assert(*keytemp>=0 && *keytemp<=2);
  
  if(*keytemp==1) 
    nuc_eos::nuc_eos_m_kt1_full(&n_in, rho, temp, ye, eps, prs, ent, cs2,
				dedt, dpderho, dpdrhoe, xa, xh, xn,
				xp, abar, zbar, mue, mun, mup, muhat,
				keyerr, &anyerr);
  else if(*keytemp==0){
    nuc_eos::nuc_eos_m_kt0_full(&n_in, rho, temp, ye, eps, prs, ent, cs2,
				dedt, dpderho, dpdrhoe, xa, xh, xn,
				xp, abar, zbar, mue, mun, mup, muhat,
				freps, keyerr, &anyerr);
  }
  else if(*keytemp==2){
    double munu = 0;
    nuc_eos::nuc_eos_m_kt2_short(&n_in, rho, temp, ye, eps, prs, ent, cs2,
				 dedt, dpderho, dpdrhoe, &munu, freps, keyerr, &anyerr);
    *xa = -1.0;
    *xh = -1.0;
    *xn = -1.0;
    *xp = -1.0;
    *abar = -1.0;
    *zbar = -1.0;
    *mue = 0.0;
    *mun = 0.0;
    *mup = 0.0;
    *muhat = 0.0;
  }

  if(anyerr!=0) std::cout << anyerr << " " << *keytemp << std::endl;
  assert(anyerr==0);
}

extern "C"
double eos_tempmin(){
	return nuc_eos::eos_tempmin;
}

extern "C"
double eos_rhomin(){
	return nuc_eos::eos_rhomin;
}

extern "C"
double eos_yemin(){
	return nuc_eos::eos_yemin;
}

extern "C"
double eos_yemax(){
	return nuc_eos::eos_yemax;
}

void trim(std::string& str)
{
  std::string::size_type pos1 = str.find_first_not_of(' ');
  std::string::size_type pos2 = str.find_last_not_of(' ');
  str = str.substr(pos1 == std::string::npos ? 0 : pos1,
    pos2 == std::string::npos ? str.length() - 1 : pos2 - pos1 + 1);
}

extern "C"
void readtable_(char* eos_filename, double *rhomin){
	std::string s(eos_filename);
	trim(s);
	char* name = new char [s.length()+1];
	std::strcpy(name,s.c_str());
	nuc_eos::nuc_eos_C_ReadTable(name);
	*rhomin = nuc_eos::eos_rhomin;
	delete[] name;
}
