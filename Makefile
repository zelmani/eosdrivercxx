
include make.inc

SOURCES=driver.cc \
        nuc_eos_press.cc \
        testeos.cc \
        readtable.cc \
        nuc_eos_press_cs2.cc \
        nuc_eos_dpdrhoe_dpderho.cc \
        nuc_eos_short.cc \
        nuc_eos_full.cc
CSOURCES= 
INCLUDES=nuc_eos.hh helpers.hh 
OBJECTS=$(SOURCES:.cc=.o )
COBJECTS=$(CSOURCES:.c=.o )

all: driver nuc_eos.a

driver: $(OBJECTS) $(COBJECTS) $(INCLUDES)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o driver $(OBJECTS) $(COBJECTS) $(HDF5LIBS) $(EXTRALIBS) 

$(OBJECTS): %.o: %.cc $(INCLUDES)
	$(CXX) $(CXXFLAGS) $(HDF5INCS) -c $< -o $@

$(COBJECTS): %.o: %.c $(INCLUDES)
	$(CC) $(CFLAGS) $(HDF5INCS) -c $< -o $@

nuc_eos.a: $(OBJECTS)
	ar r nuc_eos.a nuc_eos*.o readtable.o

clean:
	rm -f *.o
	rm -rf *.cc.*
